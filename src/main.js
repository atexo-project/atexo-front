import './assets/main.css'
import VuePlayingCard from 'vue-playing-card';

import { createApp } from 'vue'
import App from './App.vue'
import cardHand from "@/components/CardHand.vue";

createApp(App).use(VuePlayingCard).component("CardHand", cardHand).mount('#app')
